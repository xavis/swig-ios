//
//  LoadController.m
//  Swig
//
//  Created by Lion User on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoadController.h"
#import "DealSearchResults.h"
#import "BarSearchResults.h"
#import "AppDelegate.h"

@interface LoadController ()

@end

@implementation LoadController

@synthesize act;
@synthesize key;
@synthesize newview;
@synthesize contador;
@synthesize newview2;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [act setCenter:(self.view.center)];
    [self.view addSubview:act];
    [act startAnimating];
    if(contador==1){
    newview = [[BarSearchResults alloc] initWithString:self.key];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loader:) name:@"Go" object:nil];}
    else{
    
        newview2 = [[DealSearchResults alloc] initWithString:self.key NibName:@"DealSearchResults" bundle:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loader2:) name:@"Go" object:nil];}
	// Do any additional setup after loading the view.
}

-(void)loader:(NSNotification *)notification {
    
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = newview;
                        [UIView setAnimationsEnabled:oldState];
                    } 
                    completion:nil];
}


-(void)loader2:(NSNotification *)notification {
    
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = newview2;
                        [UIView setAnimationsEnabled:oldState];
                    } 
                    completion:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationPortrait));
}

@end
