//
//  Deal.h
//  Swig
//
//  Created by Lion User on 31/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Bar.h"

@interface Deal : NSObject<NSCoding>

@property NSString *nombre;
@property NSString *tipo;
@property Bar *bar;
@property NSString *fechafin;
@property NSString *horario;
@property NSString *descripcion;
@property UIImage *imagen;

@end
