//
//  Favourites.h
//  Swig
//
//  Created by Lion User on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/NSCoder.h>

@interface Favourites : NSObject<NSCoding>
@property (strong, nonatomic) NSMutableArray *FavouritesList;

@end

