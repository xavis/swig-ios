//
//  BarDetailView.m
//  Swig
//
//  Created by Lion User on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BarDetailView.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "Favourites.h"
#import "DetailMap.h"
#import "BarDealDownloader.h"
#import "DealDetailView.h"
#import "Reachability.h"

@interface BarDetailView ()

@end

@implementation BarDetailView
@synthesize bar;
@synthesize BarImage;
@synthesize direccion;
@synthesize telefono;
@synthesize mapa;
@synthesize llamar;
@synthesize titulo;
@synthesize background;
@synthesize home;
@synthesize fav;
@synthesize tipo;
@synthesize xml;
@synthesize deals;
@synthesize bar1;
@synthesize bar2;
@synthesize bar3;
@synthesize barDer;
@synthesize barIzq;
@synthesize internetActive;
@synthesize hostActive;
@synthesize connection;

int count=0;



int buttonState=0;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(self.bar.nombre){
        self.titulo.text = self.bar.nombre;
        self.titulo.layer.zPosition=2;
    }
    else{self.titulo.text=@"Bar Elegido";}
    if(self.bar.direccion){
        self.direccion.text = self.bar.direccion;}
    
    else{self.direccion.text = @"nulo";}
    if(self.bar.telefono.length>4){
        self.telefono.text = self.bar.telefono;}
    else{
            self.telefono.text = @"No hay número";
    }
    NSLog(@"Patataaaaaaaaaaa   %@",self.telefono.text);

    if(self.bar.imagen){
    
        self.BarImage.animationImages = [NSArray arrayWithObjects:    
                                                [UIImage imageNamed:@"li1.tiff"],
                                                [UIImage imageNamed:@"li2.tiff"],
                                                [UIImage imageNamed:@"li3.tiff"],
                                                [UIImage imageNamed:@"li4.tiff"],
                                                [UIImage imageNamed:@"li5.tiff"],
                                                [UIImage imageNamed:@"li6.tiff"],
                                                [UIImage imageNamed:@"li7.tiff"],
                                                [UIImage imageNamed:@"li8.tiff"],
                                                [UIImage imageNamed:@"li9.tiff"],
                                                nil];
        // all frames will execute in 1.75 seconds
        self.BarImage.animationDuration = 1.0;
        // repeat the annimation forever
        self.BarImage.animationRepeatCount = 0;
        // start animating
        [self.BarImage startAnimating];           
       
}
    else{ self.BarImage.image = nil; }
    tipo.layer.zPosition=2;
    if(bar.tipo){tipo.text=bar.tipo;} 
    else{tipo.text=@"Indeterminado";}
    
    
    Favourites *favourites = [[Favourites alloc] init];
    favourites.FavouritesList = [[NSMutableArray alloc] init];
    
    buttonState=0;
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    path = [path stringByAppendingPathComponent:@"Favoritos"];
    favourites.FavouritesList = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    
    for(int i=0;i<[favourites.FavouritesList count];i++){
        NSLog(@" A ");
        Bar *comp = [favourites.FavouritesList objectAtIndex:i];
        if([bar.nombre isEqual:comp.nombre]&&[bar.direccion isEqual:comp.direccion]){
            buttonState=1;
            NSLog(@"%@ y %@",bar,[bar isEqual:[favourites.FavouritesList objectAtIndex:i]] );
            
        }}
    
    if(buttonState==0){
        
        [fav setImage:[UIImage imageNamed:@"star1.png"]  forState:UIControlStateNormal];
        
    }
    
    else{
            
        [fav setImage:[UIImage imageNamed:@"star2.png"] forState:UIControlStateNormal];
        
        
    }
    UISwipeGestureRecognizer *Right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
    [Right setDirection:(UISwipeGestureRecognizerDirectionRight )];
    [self.view addGestureRecognizer:Right];
    
    
    
    if([self isConnectionAvailable]){
    [self.BarImage setFrame:CGRectMake(self.BarImage.frame.origin.x+80, self.BarImage.frame.origin.y+30, self.BarImage.frame.size.width/2, self.BarImage.frame.size.height)];
    UIImageView *patata = [[UIImageView alloc] initWithFrame:bar2.frame];
    
    patata.animationImages = [NSArray arrayWithObjects:    
                              [UIImage imageNamed:@"li1.png"],
                              [UIImage imageNamed:@"li2.png"],
                              [UIImage imageNamed:@"li3.png"],
                              [UIImage imageNamed:@"li4.png"],
                              [UIImage imageNamed:@"li5.png"],
                              [UIImage imageNamed:@"li6.png"],
                              [UIImage imageNamed:@"li7.png"],
                              [UIImage imageNamed:@"li8.png"],
                              [UIImage imageNamed:@"li9.png"],
                              nil];
    const float colorMasking[6] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
    // all frames will execute in 1.75 seconds
    patata.animationDuration = 1.0;
    // repeat the annimation forever
    patata.animationRepeatCount = 0;
    // start animating
    
    [bar2 setImage:patata.image forState:UIControlStateNormal];
    
    [patata setFrame:CGRectMake(-15, -20, 90, 90)];
    
    [bar2 addSubview:patata];
    patata.tag=747474;
    [patata startAnimating]; 

    
    [self performSelectorInBackground:@selector(loadimage:) withObject:self];
    
    [bar1 setEnabled:false];
    [bar2 setEnabled:false];
    [bar3 setEnabled:false];
    [barDer setEnabled:false];
    [barIzq setEnabled:false];
    
    
        [self loadDeals];   }
    
    else{ connection.text=@"No hay conexión";
    [bar2 setImage:nil forState:UIControlStateNormal];
        [BarImage stopAnimating];
    self.BarImage.animationImages=nil;
        
    [BarImage setImage:[UIImage imageNamed:@"fisnura1.jpg"]];
    } 

    
}

- (BOOL) isConnectionAvailable
{
    
    NSLog(@"Comprobando conexión");
	SCNetworkReachabilityFlags flags;
    BOOL receivedFlags;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(CFAllocatorGetDefault(), [@"google.com" UTF8String]);
    receivedFlags = SCNetworkReachabilityGetFlags(reachability, &flags);
    CFRelease(reachability);
    
    if (!receivedFlags || (flags == 0) )
    {
        return FALSE;
    } else {
		return TRUE;
	}
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationPortrait));
}

-(void)handleSwipeRight:(UISwipeGestureRecognizer *)Right{
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    UIViewController *openview = [array objectAtIndex:array.count-1];
    
    
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = openview;
                        
                        [UIView setAnimationsEnabled:oldState];
                        
                        [array removeObjectAtIndex:array.count-1];
                        appDelegate.ViewArray = [NSArray arrayWithArray:array];
                        
                        
                    } 
                    completion:nil];}



-(IBAction)homeAction:(id)sender{

    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    UIViewController *openview = [array objectAtIndex:0];
    
    [self presentModalViewController:openview animated:YES];
    
    /*[UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = openview;
                        
                        [UIView setAnimationsEnabled:oldState];
                        
                        [array removeObjectAtIndex:array.count-1];
                        appDelegate.ViewArray = nil;
    
                    } completion:nil];*/} 





-(IBAction)addRestaurantToFavourites:(id)sender{
    
    Favourites *favourites = [[Favourites alloc] init];
    
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    path = [path stringByAppendingPathComponent:@"Favoritos"];
    favourites.FavouritesList = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    if(favourites.FavouritesList==nil){
        favourites.FavouritesList = [[NSMutableArray alloc] init];}
    
    
    if(buttonState==0){         
        
        
        [favourites.FavouritesList addObject:self.bar];
        
        NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        path = [path stringByAppendingPathComponent:@"Favoritos"];
        [NSKeyedArchiver archiveRootObject:(favourites.FavouritesList) toFile:path];
        
        NSLog(@"Entra");
        buttonState=1;
        [fav setImage:[UIImage imageNamed:@"star2.png"] forState:UIControlStateNormal];
        NSLog(@"CUENTA: %d", favourites.FavouritesList.count);
        
    }
    
    else{NSLog(@"%d -- %d", buttonState, 1);
        for(int i=0;i<[favourites.FavouritesList count];i++){
            NSLog(@" A ");
            Bar *comp = [favourites.FavouritesList objectAtIndex:i];
            if([bar.nombre isEqual:comp.nombre]&&[bar.direccion isEqual:comp.direccion]){
                [favourites.FavouritesList removeObjectAtIndex:i];
                i=[favourites.FavouritesList count];
                buttonState=0;
                NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
                path = [path stringByAppendingPathComponent:@"Favoritos"];
                [NSKeyedArchiver archiveRootObject:(favourites.FavouritesList) toFile:path];
                [fav setImage:[UIImage imageNamed:@"star1.png"] forState:nil ];
                
            }         }
        
        
        
    }
    
    
}

-(void)loadimage:(UIImage*)im{
    sleep(1);
    [self.BarImage stopAnimating]; 
    self.BarImage.animationImages = nil;
    NSString *CompleteURL = self.bar.imagen;
    self.BarImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:CompleteURL]]];  
    CABasicAnimation *opacidad = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacidad.duration=2.0;
    opacidad.repeatCount=1;
    opacidad.autoreverses=NO;
    opacidad.fromValue=[NSNumber numberWithFloat:0];
    opacidad.toValue=[NSNumber numberWithFloat:1.0];
    
    [BarImage.layer addAnimation:opacidad forKey:@"AddOpacity"];
    
    [self.BarImage setFrame:CGRectMake(self.BarImage.frame.origin.x-80, self.BarImage.frame.origin.y-30, self.BarImage.frame.size.width*2, self.BarImage.frame.size.height)];
    NSLog(@"RANGO ES: %@",CompleteURL);
    // RestaurantImage.layer.cornerRadius = 30;
    BarImage.layer.masksToBounds = YES;
    BarImage.layer.borderWidth = 2;
    BarImage.layer.borderColor = CGColorCreateCopyWithAlpha(nil, 0);
    if(self.BarImage.image == NULL){
        self.BarImage.image = /*[UIImage imageNamed:@"defaultimage.png"]*/ nil;   
    }
    
    
}

-(void) viewDidAppear:(BOOL)animated{

}

-(IBAction)goMap:(id)sender{

    Bar *rest = self.bar;
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    DetailMap *newview = [[DetailMap alloc] initWithNibName:@"DetailMap" bundle:nil];
    newview.bar = rest;
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    [array addObject:self];
    appDelegate.ViewArray = [NSArray arrayWithArray:array];
    
    
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = newview;
                        [UIView setAnimationsEnabled:oldState];
                    } 
                    completion:nil];}
    


-(IBAction)call:(id)sender{

    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"]&&bar.telefono&&bar.telefono!=@"" ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",bar.telefono]]];
    } 
    
    else if(!bar.telefono||bar.telefono.length==0||bar.telefono.length==1){
        UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Este establecimiento no ofrece este servicio" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [Notpermitted show];
    }
    
    else {
        UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Tu dispositivo no soporta esta acción." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [Notpermitted show];
    }
}




-(IBAction)back:(id)sender{
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    UIViewController *openview = [array objectAtIndex:array.count-1];
    
    
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = openview;
                        
                        [UIView setAnimationsEnabled:oldState];
                        
                        [array removeObjectAtIndex:array.count-1];
                        appDelegate.ViewArray = [NSArray arrayWithArray:array];
                        
                        
                    } 
                    completion:nil];}

-(void)loadDeals{
    
   
        NSLog(@"CARGAMOS OFERTAS");
    xml = [[BarDealDownloader alloc] init];
    [xml downloadDataWithString:self.bar.barid];
    [[NSNotificationCenter defaultCenter] 
     addObserver:self 
     selector:@selector(dealimageload:)
     name:@"NewRestaurants"
     object:nil];
    

}

-(void)dealimageload:(NSNotification *)notification {
    NSLog(@"ENTRA");
    
    deals = xml.descarga.DownloadedList;
    NSLog(@"%d",deals.count);
    if(deals.count==0){
    
        [bar1 setEnabled:false];
        [bar2 setEnabled:false];
        [bar3 setEnabled:false];
        [barDer setEnabled:false];
        [barIzq setEnabled:false];
         UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
            [[window viewWithTag:747474] removeFromSuperview];
            connection.text=@"No hay ofertas";
        
    
    }
    else{
    if(deals.count>0){
        if(deals.count==1){
            Deal *b1 = [deals objectAtIndex:0];
            [bar1 setImage: b1.imagen forState:UIControlStateNormal];
            [bar2 setImage: b1.imagen forState:UIControlStateNormal];
            [bar3 setImage: b1.imagen forState:UIControlStateNormal];
            bar1.tag = 0;
            bar2.tag = 0;
            bar3.tag = 0;}
        else if(deals.count==2){
            NSLog(@"entra a 2");
            Deal *b1 = [deals objectAtIndex:0];
            [bar1 setImage: b1.imagen forState:UIControlStateNormal];
            Deal *b2 = [deals objectAtIndex:1];
            [bar2 setImage: b2.imagen forState:UIControlStateNormal];
            [bar3 setImage: b1.imagen forState:UIControlStateNormal];
            bar1.tag = 0;
            bar2.tag = 1;
            bar3.tag = 0;
            
        }
        else if(deals.count>=3){
            Deal *b1 = [deals objectAtIndex:0];
            Deal *b2 = [deals objectAtIndex:1];
            Deal *b3 = [deals objectAtIndex:2];
            [bar1 setImage: b1.imagen forState:UIControlStateNormal];
            [bar2 setImage: b2.imagen forState:UIControlStateNormal];
            [bar3 setImage: b3.imagen forState:UIControlStateNormal];
            bar1.tag = 0;
            bar2.tag = 1;
            bar3.tag = 2;
        }
    }
    
        [bar1 setEnabled:true];
        [bar2 setEnabled:true];
        [bar3 setEnabled:true];
        [barDer setEnabled:true];
        [barIzq setEnabled:true];
        
    }
    CABasicAnimation *opacidad = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacidad.duration=2.0;
    opacidad.repeatCount=1;
    opacidad.autoreverses=NO;
    opacidad.fromValue=[NSNumber numberWithFloat:0];
    opacidad.toValue=[NSNumber numberWithFloat:1.0];
    
    bar1.imageView.layer.cornerRadius=20;
    bar2.imageView.layer.cornerRadius=15;
    bar3.imageView.layer.cornerRadius=10;
    
    
    
    [bar1.layer addAnimation:opacidad forKey:@"AddOpacity"];
    [bar2.layer addAnimation:opacidad forKey:@"AddOpacity"];
    [bar3.layer addAnimation:opacidad forKey:@"AddOpacity"];
    
    UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    [[window viewWithTag:999] removeFromSuperview];
    window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    [[window viewWithTag:747474] removeFromSuperview];
    
    
}

-(IBAction)OnRightClicked:(id)sender{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.barDer.enabled = NO; //works
        self.barIzq.enabled = NO;
    });
    
    
    UIImageView *patata = [[UIImageView alloc] initWithFrame:bar2.frame];
    
    
    patata.animationImages = [NSArray arrayWithObjects:    
                              [UIImage imageNamed:@"li1.png"],
                              [UIImage imageNamed:@"li2.png"],
                              [UIImage imageNamed:@"li3.png"],
                              [UIImage imageNamed:@"li4.png"],
                              [UIImage imageNamed:@"li5.png"],
                              [UIImage imageNamed:@"li6.png"],
                              [UIImage imageNamed:@"li7.png"],
                              [UIImage imageNamed:@"li8.png"],
                              [UIImage imageNamed:@"li9.png"],
                              nil];
    const float colorMasking[6] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
    // all frames will execute in 1.75 seconds
    patata.animationDuration = 1.0;
    // repeat the annimation forever
    patata.animationRepeatCount = 0;
    // start animating
    
    [bar2 setImage:patata.image forState:UIControlStateNormal];
    
    [patata setFrame:CGRectMake(-15, -20, 90, 90)];
    
    [bar2 addSubview:patata];
    patata.tag=747474;
    [patata startAnimating]; 
    [bar2 setImage:nil forState:UIControlStateNormal];
    [bar1 setImage:nil forState:UIControlStateNormal];
    [bar3 setImage:nil forState:UIControlStateNormal];
    
    [NSThread detachNewThreadSelector:@selector(RightThread) toTarget:self withObject:nil];
    
    
    
}

-(void)RightThread{
    
    
    
        if(deals.count==1){
        bar1.tag = 0;
        bar2.tag = 0;
        bar3.tag = 0;}
    
    
    else if(deals.count==2){
        if(count==0){
            Deal *b1 = [deals objectAtIndex:0];
            Deal *b2 = [deals objectAtIndex:1];
            [bar1 setImage: b2.imagen forState:UIControlStateNormal];
            [bar2 setImage: b1.imagen forState:UIControlStateNormal];
            [bar3 setImage: b2.imagen forState:UIControlStateNormal];
            bar1.tag = 1;
            bar2.tag = 0;
            bar3.tag = 1;
            
            count=1;}
        else if(count==1){
            Deal *b1 = [deals objectAtIndex:0];
            Deal *b2 = [deals objectAtIndex:1];
            [bar1 setImage: b1.imagen forState:UIControlStateNormal];
            [bar2 setImage: b2.imagen forState:UIControlStateNormal];
            [bar3 setImage: b1.imagen forState:UIControlStateNormal];
            bar1.tag = 0;
            bar2.tag = 1;
            bar3.tag = 0;
            
            
            
            
            count=0;}
    }
    else if(deals.count>=3){
        NSMutableArray *deals2 = [[NSMutableArray alloc] initWithCapacity:deals.count];
        Deal *cambio = [deals objectAtIndex:deals.count-1];
        [deals2 addObject:cambio];
        for(int i=0;i<deals.count-1;i++){
            [deals2 addObject:[deals objectAtIndex:i]];
        }
        deals = deals2;
        
        Deal *b1 = [deals objectAtIndex:0];
        Deal *b2 = [deals objectAtIndex:1];
        Deal *b3 = [deals objectAtIndex:2];
        [bar1 setImage: b1.imagen forState:UIControlStateNormal];
        [bar2 setImage: b2.imagen forState:UIControlStateNormal];
        [bar3 setImage: b3.imagen forState:UIControlStateNormal];
        bar1.tag = 0;
        bar2.tag = 1;
        bar3.tag = 2;
        
        self.barDer.enabled = YES;
        self.barIzq.enabled = YES;
        
    }
    
    
    
    
    CABasicAnimation *opacidad = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacidad.duration=2.0;
    opacidad.repeatCount=1;
    opacidad.autoreverses=NO;
    opacidad.fromValue=[NSNumber numberWithFloat:0];
    opacidad.toValue=[NSNumber numberWithFloat:1.0];
    
    [bar1.layer addAnimation:opacidad forKey:@"AddOpacity"];
    [bar2.layer addAnimation:opacidad forKey:@"AddOpacity"];
    [bar3.layer addAnimation:opacidad forKey:@"AddOpacity"];
    UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    [[window viewWithTag:747474] removeFromSuperview];
    
}



-(IBAction)OnLeftClicked:(id)sender{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.barDer.enabled = NO; //works
        self.barIzq.enabled = NO;
    });
    UIImageView *patata = [[UIImageView alloc] initWithFrame:bar2.frame];
    
    patata.animationImages = [NSArray arrayWithObjects:    
                              [UIImage imageNamed:@"li1.png"],
                              [UIImage imageNamed:@"li2.png"],
                              [UIImage imageNamed:@"li3.png"],
                              [UIImage imageNamed:@"li4.png"],
                              [UIImage imageNamed:@"li5.png"],
                              [UIImage imageNamed:@"li6.png"],
                              [UIImage imageNamed:@"li7.png"],
                              [UIImage imageNamed:@"li8.png"],
                              [UIImage imageNamed:@"li9.png"],
                              nil];
    // all frames will execute in 1.75 seconds
    patata.animationDuration = 1.0;
    // repeat the annimation forever
    patata.animationRepeatCount = 0;
    // start animating
    
    [bar2 setImage:patata.image forState:UIControlStateNormal];
    
    [patata setFrame:CGRectMake(-15, -20, 90, 90)];
    
    [bar2 addSubview:patata];
    patata.tag=747474;
    [patata startAnimating]; 
    [bar2 setImage:nil forState:UIControlStateNormal];
    [bar1 setImage:nil forState:UIControlStateNormal];
    [bar3 setImage:nil forState:UIControlStateNormal];
    
    
    [NSThread detachNewThreadSelector:@selector(LeftThread) toTarget:self withObject:nil];
    
    
    
}

-(void)LeftThread{
    
    if(deals.count==1){
        bar1.tag = 0;
        bar2.tag = 0;
        bar3.tag = 0;}
    
    
    else if(deals.count==2){
        if(count==0){
            Deal *b1 = [deals objectAtIndex:0];
            Deal *b2 = [deals objectAtIndex:1];
            [bar1 setImage: b2.imagen forState:UIControlStateNormal];
            [bar2 setImage: b1.imagen forState:UIControlStateNormal];
            [bar3 setImage: b2.imagen forState:UIControlStateNormal];
            bar1.tag = 1;
            bar2.tag = 0;
            bar3.tag = 1;
            
            count=1;}
        else if(count==1){
            Deal *b1 = [deals objectAtIndex:0];
            Deal *b2 = [deals objectAtIndex:1];
            [bar1 setImage: b1.imagen forState:UIControlStateNormal];
            [bar2 setImage: b2.imagen forState:UIControlStateNormal];
            [bar3 setImage: b1.imagen forState:UIControlStateNormal];
            bar1.tag = 0;
            bar2.tag = 1;
            bar3.tag = 0;
            
            
            
            
            count=0;}
    }
    else if(deals.count>=3){
        Deal *cambio = [deals objectAtIndex:0];
        [deals removeObjectAtIndex:0];
        [deals insertObject:cambio atIndex:deals.count];
        Deal *b1 = [deals objectAtIndex:0];
        Deal *b2 = [deals objectAtIndex:1];
        Deal *b3 = [deals objectAtIndex:2];
        [bar1 setImage: b1.imagen forState:UIControlStateNormal];
        [bar2 setImage: b2.imagen forState:UIControlStateNormal];
        [bar3 setImage: b3.imagen forState:UIControlStateNormal];
        bar1.tag = 0;
        bar2.tag = 1;
        bar3.tag = 2;
    }
    
    
    
    
    CABasicAnimation *opacidad = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacidad.duration=2.0;
    opacidad.repeatCount=1;
    opacidad.autoreverses=NO;
    opacidad.fromValue=[NSNumber numberWithFloat:0];
    opacidad.toValue=[NSNumber numberWithFloat:1.0];
    
    [bar1.layer addAnimation:opacidad forKey:@"AddOpacity"];
    [bar2.layer addAnimation:opacidad forKey:@"AddOpacity"];
    [bar3.layer addAnimation:opacidad forKey:@"AddOpacity"];
    
    UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    [[window viewWithTag:747474] removeFromSuperview];
    
    self.barDer.enabled = YES;
    self.barIzq.enabled = YES;
}


-(IBAction)onBarClick:(id)sender{
    
    UIButton *send = sender;
    
    Deal *rest = [self.deals objectAtIndex: send.tag];
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    DealDetailView *newview = [[DealDetailView alloc] initWithNibName:@"DealDetailView" bundle:nil];
    rest.bar = self.bar;
    newview.deal = rest;
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    [array addObject:self];
    appDelegate.ViewArray = [NSArray arrayWithArray:array];
    
    
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = newview;
                        [UIView setAnimationsEnabled:oldState];
                    } 
                    completion:nil];}


@end
