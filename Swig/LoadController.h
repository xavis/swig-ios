//
//  LoadController.h
//  Swig
//
//  Created by Lion User on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DealSearchResults.h"
#import "BarSearchResults.h"

@interface LoadController : UIViewController

@property IBOutlet UIActivityIndicatorView *act;
@property NSString *key;
@property BarSearchResults *newview; 
@property DealSearchResults *newview2;
@property int contador;
@end
