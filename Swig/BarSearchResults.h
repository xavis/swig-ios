//
//  BarSearchResults.h
//  Swig
//
//  Created by Lion User on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BarDownloader.h"

@interface BarSearchResults : UITableViewController

@property (nonatomic, strong) NSString *xmlkeystring;
@property (nonatomic, strong) NSMutableArray *xmlkeyarray;
@property (nonatomic, strong) BarDownloader *xml;
@property (nonatomic, strong) NSMutableArray *Restaurants;
@property (nonatomic, assign) int pagina;
-(id) initWithString:(NSString *) key;
-(id) initWithArray:(NSMutableArray *) key;

- (void)someMethodToReloadTable:(NSNotification *)notification;
- (void)loadMore;
- (void)loadPrev;

-(void)handleSwipeRight:(UISwipeGestureRecognizer *)Right;

@end
