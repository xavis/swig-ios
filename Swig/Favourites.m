//
//  Favourites.m
//  Swig
//
//  Created by Lion User on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Favourites.h"
#import <Foundation/NSCoder.h>

@implementation Favourites
@synthesize FavouritesList;

- (void)encodeWithCoder:(NSCoder *)coder {
    
    [coder encodeObject:self.FavouritesList forKey:@"Favourites"];
    
}

- (id)initWithCoder:(NSCoder *)coder {
    
    if ((self = [super init])) {
        
        self.FavouritesList = [coder decodeObjectForKey:@"Favourites"];
    }
    
    return self;
    
}



@end