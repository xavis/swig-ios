//
//  DetailMap.m
//  Swig
//
//  Created by Lion User on 21/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DetailMap.h"
#import "MyAnnotationClass.h"
#import <MapKit/Mapkit.h>
#import "Bar.h"
#import "AppDelegate.h"

@implementation DetailMap
@synthesize mapView;
@synthesize bar;
UIButton *boton;


- (void)viewDidLoad
{   [super viewDidLoad];
    
     Location.showsUserLocation = TRUE;
    
    CLLocationCoordinate2D location;
    NSString *lat = [bar.latitud stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    lat = [lat stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *lon = [bar.longitud stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    lon = [lon stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"a%@a a%@a",lat, lon);
    location.latitude = (double) [lat doubleValue];
    location.longitude =(double) [lon doubleValue];
    MapViewAnnotation *newAnnotation = [[MapViewAnnotation alloc] initWithTitle:[NSString stringWithFormat:@"%@",bar.nombre] andCoordinate:location];
    NSLog(@"%f %f",location.latitude, location.longitude);
    [self.mapView addAnnotation:newAnnotation];
    Location.mapType = MKMapTypeHybrid;
    Location.showsUserLocation = TRUE;
    [mapView setCenterCoordinate:CLLocationCoordinate2DMake(0,0) animated:YES];
    mapView.showsUserLocation=YES;
    [mapView setRegion:MKCoordinateRegionMake(CLLocationCoordinate2DMake(location.latitude, location.longitude), MKCoordinateSpanMake(0.1, 0.1))];
    
    UIButton *subut = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    subut.frame = CGRectMake(0, 0, 320, 30);
    [subut setTitle: @"Volver Atrás" forState: UIControlStateNormal];
    [subut addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside]; 
    [mapView addSubview:subut];

    
    
	// Add the annotation to our map view
	
	
	//[newAnnotation release];
}


-(IBAction)buttonClicked:(id)sender{

    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    UIViewController *openview = [array objectAtIndex:array.count-1];
    
    
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = openview;
                        
                        [UIView setAnimationsEnabled:oldState];
                        
                        [array removeObjectAtIndex:array.count-1];
                        appDelegate.ViewArray = [NSArray arrayWithArray:array];
                        
                        
                    } 
                    completion:nil];


}

- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views
{
	MKAnnotationView *annotationView = [views objectAtIndex:0];
	id <MKAnnotation> mp = [annotationView annotation];
	MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([mp coordinate], 1500, 1500);
	[mv setRegion:region animated:YES];
	[mv selectAnnotation:mp animated:YES];
}
@end
