//
//  Descarga.h
//  Swig
//
//  Created by Lion User on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/NSCoder.h>

@interface Descarga : NSObject<NSCoding>
@property (strong, nonatomic) NSMutableArray *DownloadedList;

@end
