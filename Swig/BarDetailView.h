//
//  BarDetailView.h
//  Swig
//
//  Created by Lion User on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bar.h"
#import "BarDealDownloader.h"
@class Reachability;

@interface BarDetailView : UIViewController{
    Reachability* internetReachable;
    Reachability* hostReachable;}

 
@property Bar *bar;
@property IBOutlet UIButton *mapa;
@property IBOutlet UIButton *llamar;
@property IBOutlet UIButton *home;
@property IBOutlet UIButton *fav;

@property IBOutlet UILabel *titulo;
@property IBOutlet UILabel *direccion;
@property IBOutlet UILabel *telefono;
@property IBOutlet UILabel *tipo;

@property IBOutlet UIImageView *BarImage;
@property IBOutlet UIImageView *background;

@property IBOutlet UIButton *bar1;
@property IBOutlet UIButton *bar2;
@property IBOutlet UIButton *bar3;
@property IBOutlet UIButton *barDer;
@property IBOutlet UIButton *barIzq;
@property BOOL internetActive;
@property BOOL hostActive;
@property IBOutlet UILabel *connection;
@property NSMutableArray *deals;

@property BarDealDownloader *xml;

-(void)handleSwipeRight:(UISwipeGestureRecognizer *)Right;

-(IBAction)homeAction:(id)sender;

-(void)loadDeals;
-(void) checkNetworkStatus:(NSNotification *)notice;


@end
