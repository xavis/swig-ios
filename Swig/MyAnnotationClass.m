//
//  MyAnnotationClass.m
//  Swig
//
//  Created by Lion User on 21/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import "MyAnnotationClass.h"

@implementation MapViewAnnotation

@synthesize title, coordinate;

- (id)initWithTitle:(NSString *)ttl andCoordinate:(CLLocationCoordinate2D)c2d {
	self= [super init];
	title = ttl;
	coordinate = c2d;
	return self;
}

@end