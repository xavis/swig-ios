//
//  DetailMap.h
//  Swig
//
//  Created by Lion User on 21/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Bar.h"

@interface DetailMap : UIViewController<MKMapViewDelegate>
{
    IBOutlet MKMapView *Location;    
    
}

@property (nonatomic, retain) Bar *bar;

@property (nonatomic, retain) IBOutlet MKMapView *mapView;



@end