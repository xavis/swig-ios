//
//  ViewController.m
//  Swig
//
//  Created by Lion User on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "iToast.h"
#import "LoadController.h"
#import "AppDelegate.h"
#import "FavController.h"
#import "BarSearchResults.h"
#import <QuartzCore/QuartzCore.h>
#import "BarDetailView.h"
#import "Bar.h"
#import "AppDelegate.h"
#import "Reachability.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize Fav;
@synthesize bar1;
@synthesize bar2;
@synthesize bar3;
@synthesize barDer;
@synthesize barIzq;
@synthesize selectBar;
@synthesize selectOferta;
@synthesize premium;
@synthesize entrada;
@synthesize toast;
@synthesize buscabar;
@synthesize bares;
@synthesize downloader;
@synthesize backboton;
@synthesize loadimage;
@synthesize internetActive;
@synthesize hostActive;
@synthesize conexion;

int opcion=1;
int maxcount=0;
int numBares=0;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] 
     addObserver:self 
     selector:@selector(showText:)
     name:UITextFieldTextDidChangeNotification 
     object:entrada];
    entrada.autocorrectionType = UITextAutocorrectionTypeNo;
    
    if([self isConnectionAvailable]){
    loadimage.animationImages = [NSArray arrayWithObjects:    
                              [UIImage imageNamed:@"loadscreen1.png"],
                              [UIImage imageNamed:@"loadscreen2.png"],
                              nil];
    // all frames will execute in 1.75 seconds
    loadimage.animationDuration = 0.35;
    // repeat the annimation forever
    loadimage.animationRepeatCount = 0;
    // start animating
    
    loadimage.tag = 999;
    
    [loadimage startAnimating];   
    
    
    
        [self barload:nil];}
    
    else{
        NSLog(@"ENTRA AL BARLOAD NO");
        [self.loadimage removeFromSuperview];
        conexion.text=@"No hay conexión";
        
        
    }
    [barDer addTarget:self action:@selector(OnRightClicked:) forControlEvents:UIControlEventTouchDown];
    [barDer addTarget:self action:nil forControlEvents:UIControlEventTouchDownRepeat];
    [barIzq addTarget:self action:@selector(OnLeftClicked:) forControlEvents:UIControlEventTouchUpInside];
        
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationPortrait));
}

-(void) viewWillAppear:(BOOL)animated{
    
    // check for internet connection
    
    
    // now patiently wait for the notification
    
}


- (BOOL) isConnectionAvailable
{
	SCNetworkReachabilityFlags flags;
    BOOL receivedFlags;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(CFAllocatorGetDefault(), [@"dipinkrishna.com" UTF8String]);
    receivedFlags = SCNetworkReachabilityGetFlags(reachability, &flags);
    CFRelease(reachability);
    
    if (!receivedFlags || (flags == 0) )
    {
        return FALSE;
    } else {
		return TRUE;
	}
}
 

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
   
    UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    [[window viewWithTag:70457] removeFromSuperview];
    [textField resignFirstResponder];
    if([self isConnectionAvailable]){[self Go:self];}
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No hay conexión" 
                                                        message:@"Debes tener conexión a internet para usar este servicio" 
                                                       delegate:nil 
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    return YES;
}

-(IBAction)backButtonAction:(id)sender{
    UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];

    [[window viewWithTag:70457] removeFromSuperview];
    [entrada resignFirstResponder];
}

- (void) showText:(id)selector{
    
    UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    
    
    
    [[window viewWithTag:70457] removeFromSuperview];
  
    
       [[iToast makeText:NSLocalizedString(entrada.text, @"")] show];
    

}

- (IBAction)Go:(id)sender{
    
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    LoadController *newview = [[LoadController alloc] init];
    newview.key = entrada.text;
    NSArray *array = [[NSArray alloc] initWithObjects:self, nil];
    appDelegate.ViewArray = array;
    [UIView transitionWithView:window2 
     duration:0.5
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^(void) {
         BOOL oldState = [UIView areAnimationsEnabled];
         [UIView setAnimationsEnabled:NO];
         newview.contador = opcion;
         window2.rootViewController = newview;
         [UIView setAnimationsEnabled:oldState];
     } 
     completion:nil];
    
    
}

- (IBAction)GoFav:(id)sender{
    
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    FavController *newview = [[FavController alloc] init];
    NSArray *array = [[NSArray alloc] initWithObjects:self, nil];
    appDelegate.ViewArray = array;
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = newview;
                        [UIView setAnimationsEnabled:oldState];
                    } 
                    completion:nil];
    
    
}

- (IBAction)ChangeUItoOfertas:(id)sender{
    if(opcion==1){
        [buscabar setImage:[UIImage imageNamed:@"busca_ofertas.png"]];
        opcion=2;
        entrada.text=@"Ciudad, provincia, comunidad...";}
}

- (IBAction)ChangeUItoBar:(id)sender{
    
    if(opcion==2){
        [buscabar setImage:[UIImage imageNamed:@"busca_bares.png"]];
        opcion=1;
    entrada.text=@"Ciudad, nombre, tipo...";}
    
} 

-(void)barload:(id)sender {
    
    self.downloader = [[MainBarDownloader alloc] init];
    [self.downloader downloadDataWithString:@""];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(barimageload:) name:@"NewBares" object:nil];
    

}

-(void)barimageload:(NSNotification *)notification {
    NSLog(@"ENTRA");

    bares = downloader.descarga.DownloadedList;
    NSLog(@"%d",bares.count);
    if(bares.count>0){
        if(bares.count==1){
            Bar *b1 = [bares objectAtIndex:0];
            bar1.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b1.imagen]]];
            bar2.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b1.imagen]]];
            bar3.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b1.imagen]]];
            bar1.tag = 0;
            bar2.tag = 0;
            bar3.tag = 0;}
        else if(bares.count==2){
            NSLog(@"entra a 2");
            Bar *b1 = [bares objectAtIndex:0];
            [bar1 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b1.imagen]]]forState:UIControlStateNormal];
            Bar *b2 = [bares objectAtIndex:1];
            [bar2 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b2.imagen]]]forState:UIControlStateNormal];
            [bar3 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b1.imagen]]]forState:UIControlStateNormal];
            bar1.tag = 0;
            bar2.tag = 1;
            bar3.tag = 0;

        }
        else if(bares.count>=3){
                Bar *b1 = [bares objectAtIndex:0];
                Bar *b2 = [bares objectAtIndex:1];
                Bar *b3 = [bares objectAtIndex:2];
            [bar1 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b1.imagen]]]forState:UIControlStateNormal];
            [bar2 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b2.imagen]]]forState:UIControlStateNormal];
            [bar3 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b3.imagen]]]forState:UIControlStateNormal];
            bar1.tag = 0;
            bar2.tag = 1;
            bar3.tag = 2;
            }
    }
    CABasicAnimation *opacidad = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacidad.duration=2.0;
    opacidad.repeatCount=1;
    opacidad.autoreverses=NO;
    opacidad.fromValue=[NSNumber numberWithFloat:0];
    opacidad.toValue=[NSNumber numberWithFloat:1.0];
    
    bar1.imageView.layer.cornerRadius=20;
    bar2.imageView.layer.cornerRadius=15;
    bar3.imageView.layer.cornerRadius=10;

[bar1.layer addAnimation:opacidad forKey:@"AddOpacity"];
[bar2.layer addAnimation:opacidad forKey:@"AddOpacity"];
[bar3.layer addAnimation:opacidad forKey:@"AddOpacity"];
    
    UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    [[window viewWithTag:999] removeFromSuperview];


}
    
    -(IBAction)OnRightClicked:(id)sender{
        dispatch_async(dispatch_get_main_queue(), ^{
            self.barDer.enabled = NO; //works
            self.barIzq.enabled = NO;
        });
        

        UIImageView *patata = [[UIImageView alloc] initWithFrame:bar2.frame];
        
        
        patata.animationImages = [NSArray arrayWithObjects:    
                                  [UIImage imageNamed:@"li1.png"],
                                  [UIImage imageNamed:@"li2.png"],
                                  [UIImage imageNamed:@"li3.png"],
                                  [UIImage imageNamed:@"li4.png"],
                                  [UIImage imageNamed:@"li5.png"],
                                  [UIImage imageNamed:@"li6.png"],
                                  [UIImage imageNamed:@"li7.png"],
                                  [UIImage imageNamed:@"li8.png"],
                                  [UIImage imageNamed:@"li9.png"],
                                  nil];
        const float colorMasking[6] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
        // all frames will execute in 1.75 seconds
        patata.animationDuration = 1.0;
        // repeat the annimation forever
        patata.animationRepeatCount = 0;
        // start animating
        
        [bar2 setImage:patata.image forState:UIControlStateNormal];
        
        [patata setFrame:CGRectMake(-15, -20, 90, 90)];
        
        [bar2 addSubview:patata];
        patata.tag=747474;
        [patata startAnimating]; 
        [bar2 setImage:nil forState:UIControlStateNormal];
        [bar1 setImage:nil forState:UIControlStateNormal];
        [bar3 setImage:nil forState:UIControlStateNormal];
    
     [NSThread detachNewThreadSelector:@selector(RightThread) toTarget:self withObject:nil];
        
        

        }

-(void)RightThread{

    if(bares.count==1){
        bar1.tag = 0;
        bar2.tag = 0;
        bar3.tag = 0;}
    
    
    else if(bares.count==2){
        if(maxcount==0){
            Bar *b1 = [bares objectAtIndex:0];
            Bar *b2 = [bares objectAtIndex:1];
            [bar1 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b2.imagen]]]forState:UIControlStateNormal];
            [bar2 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b1.imagen]]]forState:UIControlStateNormal];
            [bar3 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b2.imagen]]]forState:UIControlStateNormal];
            bar1.tag = 1;
            bar2.tag = 0;
            bar3.tag = 1;
            
            maxcount=1;}
        else if(maxcount==1){
            Bar *b1 = [bares objectAtIndex:0];
            Bar *b2 = [bares objectAtIndex:1];
            [bar1 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b1.imagen]]]forState:UIControlStateNormal];
            [bar2 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b2.imagen]]]forState:UIControlStateNormal];
            [bar3 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b1.imagen]]]forState:UIControlStateNormal];
            bar1.tag = 0;
            bar2.tag = 1;
            bar3.tag = 0;
            
            
            
            
            maxcount=0;}
    }
    else if(bares.count>=3){
        NSMutableArray *bares2 = [[NSMutableArray alloc] initWithCapacity:bares.count];
        Bar *cambio = [bares objectAtIndex:bares.count-1];
        [bares2 addObject:cambio];
        for(int i=0;i<bares.count-1;i++){
            [bares2 addObject:[bares objectAtIndex:i]];
        }
        bares = bares2;
        
        Bar *b1 = [bares objectAtIndex:0];
        Bar *b2 = [bares objectAtIndex:1];
        Bar *b3 = [bares objectAtIndex:2];
        [bar1 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b1.imagen]]]forState:UIControlStateNormal];
        [bar2 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b2.imagen]]]forState:UIControlStateNormal];
        [bar3 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b3.imagen]]]forState:UIControlStateNormal];
        bar1.tag = 0;
        bar2.tag = 1;
        bar3.tag = 2;
        
        self.barDer.enabled = YES;
        self.barIzq.enabled = YES;
        
    }
    
    
    
    
    CABasicAnimation *opacidad = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacidad.duration=2.0;
    opacidad.repeatCount=1;
    opacidad.autoreverses=NO;
    opacidad.fromValue=[NSNumber numberWithFloat:0];
    opacidad.toValue=[NSNumber numberWithFloat:1.0];
    
    [bar1.layer addAnimation:opacidad forKey:@"AddOpacity"];
    [bar2.layer addAnimation:opacidad forKey:@"AddOpacity"];
    [bar3.layer addAnimation:opacidad forKey:@"AddOpacity"];
    UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    [[window viewWithTag:747474] removeFromSuperview];
    
}



-(IBAction)OnLeftClicked:(id)sender{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.barDer.enabled = NO; //works
        self.barIzq.enabled = NO;
    });
    UIImageView *patata = [[UIImageView alloc] initWithFrame:bar2.frame];
    
    patata.animationImages = [NSArray arrayWithObjects:    
                              [UIImage imageNamed:@"li1.png"],
                              [UIImage imageNamed:@"li2.png"],
                              [UIImage imageNamed:@"li3.png"],
                              [UIImage imageNamed:@"li4.png"],
                              [UIImage imageNamed:@"li5.png"],
                              [UIImage imageNamed:@"li6.png"],
                              [UIImage imageNamed:@"li7.png"],
                              [UIImage imageNamed:@"li8.png"],
                              [UIImage imageNamed:@"li9.png"],
                              nil];
    // all frames will execute in 1.75 seconds
    patata.animationDuration = 1.0;
    // repeat the annimation forever
    patata.animationRepeatCount = 0;
    // start animating
    
    [bar2 setImage:patata.image forState:UIControlStateNormal];
    
    [patata setFrame:CGRectMake(-15, -20, 90, 90)];
    
    [bar2 addSubview:patata];
    patata.tag=747474;
    [patata startAnimating]; 
    [bar2 setImage:nil forState:UIControlStateNormal];
    [bar1 setImage:nil forState:UIControlStateNormal];
    [bar3 setImage:nil forState:UIControlStateNormal];

    
    [NSThread detachNewThreadSelector:@selector(LeftThread) toTarget:self withObject:nil];
   
    
    
}

-(void)LeftThread{

    if(bares.count==1){
        bar1.tag = 0;
        bar2.tag = 0;
        bar3.tag = 0;}
    
    
    else if(bares.count==2){
        if(maxcount==0){
            Bar *b1 = [bares objectAtIndex:0];
            Bar *b2 = [bares objectAtIndex:1];
            [bar1 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b2.imagen]]]forState:UIControlStateNormal];
            [bar2 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b1.imagen]]]forState:UIControlStateNormal];
            [bar3 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b2.imagen]]]forState:UIControlStateNormal];
            bar1.tag = 1;
            bar2.tag = 0;
            bar3.tag = 1;
            
            maxcount=1;}
        else if(maxcount==1){
            Bar *b1 = [bares objectAtIndex:0];
            Bar *b2 = [bares objectAtIndex:1];
            [bar1 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b1.imagen]]]forState:UIControlStateNormal];
            [bar2 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b2.imagen]]]forState:UIControlStateNormal];
            [bar3 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b1.imagen]]]forState:UIControlStateNormal];
            bar1.tag = 0;
            bar2.tag = 1;
            bar3.tag = 0;
            
            
            
            
            maxcount=0;}
    }
    else if(bares.count>=3){
        Bar *cambio = [bares objectAtIndex:0];
        [bares removeObjectAtIndex:0];
        [bares insertObject:cambio atIndex:bares.count];
        Bar *b1 = [bares objectAtIndex:0];
        Bar *b2 = [bares objectAtIndex:1];
        Bar *b3 = [bares objectAtIndex:2];
        [bar1 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b1.imagen]]]forState:UIControlStateNormal];
        [bar2 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b2.imagen]]]forState:UIControlStateNormal];
        [bar3 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:b3.imagen]]]forState:UIControlStateNormal];
        bar1.tag = 0;
        bar2.tag = 1;
        bar3.tag = 2;
    }
    
    
    
    
    CABasicAnimation *opacidad = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacidad.duration=2.0;
    opacidad.repeatCount=1;
    opacidad.autoreverses=NO;
    opacidad.fromValue=[NSNumber numberWithFloat:0];
    opacidad.toValue=[NSNumber numberWithFloat:1.0];
    
    [bar1.layer addAnimation:opacidad forKey:@"AddOpacity"];
    [bar2.layer addAnimation:opacidad forKey:@"AddOpacity"];
    [bar3.layer addAnimation:opacidad forKey:@"AddOpacity"];
    
    UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    [[window viewWithTag:747474] removeFromSuperview];

    self.barDer.enabled = YES;
    self.barIzq.enabled = YES;
}


-(IBAction)onBarClick:(id)sender{

    UIButton *send = sender;
    
    Bar *rest = [self.bares objectAtIndex: send.tag];
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    BarDetailView *newview = [[BarDetailView alloc] initWithNibName:@"BarDetailView" bundle:nil];
    newview.bar= rest;
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    [array addObject:self];
    appDelegate.ViewArray = [NSArray arrayWithArray:array];
    
    
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = newview;
                        [UIView setAnimationsEnabled:oldState];
                    } 
                    completion:nil];}
    


    

@end

