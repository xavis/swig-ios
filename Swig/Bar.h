//
//  Bar.h
//  Swig
//
//  Created by Lion User on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bar : NSObject<NSCoding>

@property NSString *nombre;
@property NSString *tipo;
@property NSString *provincia;
@property NSString *localidad;
@property NSString *direccion;
@property NSString *latitud;
@property NSString *longitud;
@property NSString *imagen;
@property NSString *telefono;
@property NSString *barid;

@end
 