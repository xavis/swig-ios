//
//  ViewController.h
//  Swig
//
//  Created by Lion User on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainBarDownloader.h"
#import "iToast.h"
@class Reachability;

@interface 
ViewController : UIViewController <UITextFieldDelegate>{
    Reachability* internetReachable;
    Reachability* hostReachable;}

@property IBOutlet UIButton *barIzq;
@property IBOutlet UIButton *barDer;
@property NSMutableArray *bares;

@property BOOL internetActive;
@property BOOL hostActive;

@property IBOutlet UIButton *selectBar;

@property (nonatomic, strong) MainBarDownloader *downloader;
@property IBOutlet UIButton *selectOferta;

@property IBOutlet UIButton *Fav;

@property IBOutlet UITextField *entrada;

@property IBOutlet UIButton *bar1;
@property IBOutlet UIButton *bar2;
@property IBOutlet UIButton *bar3;
@property IBOutlet UIButton *backboton;

@property IBOutlet UIImageView *buscabar;
@property IBOutlet UIImageView *loadimage;
@property IBOutlet UILabel *conexion;

@property iToast *toast;

@property int opcion;
@property int numBares;



@property NSArray *premium;

-(void)barload:(id)sender;
-(void) checkNetworkStatus:(NSNotification *)notice;


@end
