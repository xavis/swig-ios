//
//  BarDealDownloader.h
//  Swig
//
//  Created by Lion User on 02/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

//
//  BarDownloader.h
//  Swig
//
//  Created by Lion User on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deal.h"
#import "Descarga.h"

@protocol BarDealDownloaderDelegate <NSObject>

- (void) ListOfRestaurantsDownloaded:(NSArray *)restaurantsList;

@end

@interface BarDealDownloader: NSObject <NSXMLParserDelegate>
{
    NSString *searchKeyString;
    NSMutableArray *searchKeyArray;
    NSURLConnection *mConnection;
    NSMutableData *mReceivedData;    
    Deal *CurrentRestaurant;
    Deal *OtherRestaurant;
    NSMutableString *rCurrentString;
    
    BOOL rIsDownloadingData;
}

- (void) downloadDataWithString:(NSString *) key;
- (void) downloadDataWithArray:(NSMutableArray *) key;

@property (nonatomic, strong) Descarga *descarga;
@property (nonatomic) BOOL *ready;
@property (nonatomic, weak) id <BarDealDownloaderDelegate>delegate;
@property (nonatomic, strong) NSMutableArray *names;

@end
