//
//  DealDownloader.m
//  Swig
//
//  Created by Lion User on 31/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import "DealDownloader.h"
#import "Descarga.h"
#import "Bar.h"


@implementation DealDownloader

@synthesize delegate;
@synthesize descarga;
@synthesize ready;
@synthesize names;



- (void) downloadDataWithString:(NSString *) key
{    
    // We dont want to download several times, while we are actually donwloading the data or parsing the data
    self.ready = NO;		
    if (rIsDownloadingData == YES)
        return;
    searchKeyString = key;
    NSString *direccionurl1 = @"http://www.swagger.hostoi.com/deal_search.php?key=";
    searchKeyString = [searchKeyString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString *direcciontotal =[NSString stringWithFormat: @"%@%@",direccionurl1,searchKeyString];
    NSLog(direcciontotal);
    NSURL *url = [NSURL URLWithString:direcciontotal];
    NSLog([NSString stringWithContentsOfURL:url]);
    mConnection = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:url] delegate:self];
    NSLog(@"Conexión2");
    
    if (mConnection != nil)
    {
        mReceivedData = [[NSMutableData alloc] init]; 
        rIsDownloadingData = YES;
        NSLog(@"Conexión3");
    }}


#pragma mark - URLConnection delegate

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    NSLog(@"Conexión3");
    [mReceivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    NSLog(@"Succeeded! Received %d bytes of data",[mReceivedData length]);
    NSString *string = [[NSString alloc] initWithData:mReceivedData encoding:NSUTF32StringEncoding];
    NSLog(@"Recevied XML: %@", string);
    
    
    descarga = [[Descarga alloc] init];
    descarga.DownloadedList = [[NSMutableArray alloc] init];
    
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:mReceivedData];
    xmlParser.delegate = self;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 
                                             (unsigned long)NULL), ^(void) {
        [xmlParser parse];
    });
    
}

#pragma mark - XML Parser delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    if ( [elementName isEqualToString:@"oferta"]) 
    {CurrentRestaurant = [[Deal alloc] init];
        
        NSLog(@"Creamos Bar");}
    
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string 
{
    if (!rCurrentString) 
        rCurrentString = [[NSMutableString alloc] init];
    
    [rCurrentString appendString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName 

{   
    if ( [elementName isEqualToString:@"nombre"]) 
    {
        CurrentRestaurant.nombre = [rCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSLog(@"Añadimos nombre");
    }
    
    else if ( [elementName isEqualToString:@"tipo"]) 
    {
        CurrentRestaurant.tipo = [rCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSLog(@"Añadimos tipo");
    }   
    
    else if ( [elementName isEqualToString:@"hora"]) 
    {
        CurrentRestaurant.horario = [rCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSLog(@"Añadimos provincia");
    }   
    
    else if ( [elementName isEqualToString:@"hasta"]) 
    {
        CurrentRestaurant.fechafin = [rCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSLog(@"Añadimos telefono");
    } 
    
    else if ( [elementName isEqualToString:@"id_bar"]) 
    {
        Bar *bar = [[Bar alloc] init];
        bar.nombre = @"Indeterminado";
        CurrentRestaurant.bar = bar; }   
    
    else if ( [elementName isEqualToString:@"descripcion"]) 
    {
        CurrentRestaurant.descripcion = [rCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSLog(@"Añadimos descripcion");}   
        
        
    else if ( [elementName isEqualToString:@"oferta"]) 
    {
        
        if(CurrentRestaurant.tipo==@"cerveza"){        CurrentRestaurant.imagen=[UIImage imageNamed:@"tipoCerveza"];}
        else if(CurrentRestaurant.tipo==@"vino"){        CurrentRestaurant.imagen=[UIImage imageNamed:@"tipoVino"];}
        else if(CurrentRestaurant.tipo==@"copa"){        CurrentRestaurant.imagen=[UIImage imageNamed:@"tipoCopa"];}
        else if(CurrentRestaurant.tipo==@"chupito"){        CurrentRestaurant.imagen=[UIImage imageNamed:@"tipoChupito"];}
        else if(CurrentRestaurant.tipo==@"tapa"){        CurrentRestaurant.imagen=[UIImage imageNamed:@"tipoTapa"];}
        else if(CurrentRestaurant.tipo==@"varios"){        CurrentRestaurant.imagen=[UIImage imageNamed:@"tipoVarios"];}
        else{CurrentRestaurant.imagen=[UIImage imageNamed:@"aBar.png"];}
        [descarga.DownloadedList addObject:CurrentRestaurant];        
        CurrentRestaurant = nil;
        
        NSLog(@"Cerramos Retaurante");
        names = nil;
        
    }
    
    rCurrentString = nil;
    }

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    if (self.delegate != nil)
        [self.delegate ListOfRestaurantsDownloaded:[NSArray arrayWithArray: descarga.DownloadedList]];
    NSLog(@"TAMAÑO DE VECTOR: %d",[descarga.DownloadedList count]); 
    NSArray *path =
	NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    [NSKeyedArchiver archiveRootObject:(descarga.DownloadedList) toFile:[NSString stringWithFormat:@"%@/DownloadedRestaurants",[path objectAtIndex:0]]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewRestaurants" object:nil];
    descarga.DownloadedList = nil;
}

@end
