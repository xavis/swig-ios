//
//  DealDetailView.h
//  Swig
//
//  Created by Lion User on 01/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Deal.h"

@interface DealDetailView : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *goBar;
@property (strong, nonatomic) IBOutlet UILabel *dealtitle;
@property (strong, nonatomic) IBOutlet UILabel *desc;
@property (strong, nonatomic) IBOutlet UILabel *horario;
@property (strong, nonatomic) IBOutlet UILabel *fecha;
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet UIButton *home;
@property (strong, nonatomic) Deal *deal;

-(void)handleSwipeRight:(UISwipeGestureRecognizer *)Right;
-(IBAction)homeAction:(id)sender;
-(IBAction)back:(id)sender;
-(IBAction)goToBar:(id)sender;


@end
