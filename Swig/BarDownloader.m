//
//  BarDownloader.m
//  Swig
//
//  Created by Lion User on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import "BarDownloader.h"
#import "Descarga.h"


@implementation BarDownloader

@synthesize delegate;
@synthesize descarga;
@synthesize ready;
@synthesize names;



- (void) downloadDataWithString:(NSString *) key
{    
    // We dont want to download several times, while we are actually donwloading the data or parsing the data
    self.ready = NO;		
    if (rIsDownloadingData == YES)
        return;
    searchKeyString = key;
    NSString *direccionurl1 = @"http://www.swagger.hostoi.com/bar_search.php?key=";
    searchKeyString = [searchKeyString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString *direcciontotal =[NSString stringWithFormat: @"%@%@",direccionurl1,searchKeyString];
    NSLog(direcciontotal);
    NSURL *url = [NSURL URLWithString:direcciontotal];
    NSLog([NSString stringWithContentsOfURL:url]);
    mConnection = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:url] delegate:self];
    NSLog(@"Conexión2");
    
    if (mConnection != nil)
    {
        mReceivedData = [[NSMutableData alloc] init]; 
        rIsDownloadingData = YES;
        NSLog(@"Conexión3");
    }}


#pragma mark - URLConnection delegate

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    NSLog(@"Conexión3");
    [mReceivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    NSLog(@"Succeeded! Received %d bytes of data",[mReceivedData length]);
    NSString *string = [[NSString alloc] initWithData:mReceivedData encoding:NSUTF32StringEncoding];
    NSLog(@"Recevied XML: %@", string);
    
    
    descarga = [[Descarga alloc] init];
    descarga.DownloadedList = [[NSMutableArray alloc] init];
    
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:mReceivedData];
    xmlParser.delegate = self;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 
                                             (unsigned long)NULL), ^(void) {
        [xmlParser parse];
    });
    
}

#pragma mark - XML Parser delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    if ( [elementName isEqualToString:@"bar"]) 
    {CurrentRestaurant = [[Bar alloc] init];
    
        NSLog(@"Creamos Bar");}
    
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string 
{
    if (!rCurrentString) 
        rCurrentString = [[NSMutableString alloc] init];
    
    [rCurrentString appendString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName 

{   
    
if ( [elementName isEqualToString:@"CoordX"]) 
{
    CurrentRestaurant.longitud = rCurrentString;
    NSLog(@"Añadimos longitud");
}
    
else if ( [elementName isEqualToString:@"CoordY"]) 
{
    CurrentRestaurant.latitud = rCurrentString;
    NSLog(@"Añadimos latitud");
}
    
else if ( [elementName isEqualToString:@"direccion"]) 
{
    CurrentRestaurant.direccion = rCurrentString;
    NSLog(@"Añadimos direccion");
}
    
else if ( [elementName isEqualToString:@"localidad"]) 
{
    CurrentRestaurant.localidad = [rCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"Añadimos localidad");
}
    
else if ( [elementName isEqualToString:@"nombre"]) 
{
    CurrentRestaurant.nombre = [rCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"Añadimos nombre");
}
    
else if ( [elementName isEqualToString:@"tipo"]) 
{
    CurrentRestaurant.tipo = [rCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"Añadimos tipo");
}   
    
else if ( [elementName isEqualToString:@"provincia"]) 
{
    CurrentRestaurant.provincia = [rCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"Añadimos provincia");
}   
    
else if ( [elementName isEqualToString:@"telefono"]) 
{
    CurrentRestaurant.telefono = [rCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"Añadimos telefono");
} 
    
else if ( [elementName isEqualToString:@"id"]) 
{
    CurrentRestaurant.barid = [rCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"Añadimos ID");
} 

   
else if ( [elementName isEqualToString:@"imagen"]) 
{
    
    NSString *url = [rCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    CurrentRestaurant.imagen=url;
    NSLog(@"Añadimos imagen con imagen %@",url);
}        
    
    
 if ( [elementName isEqualToString:@"bar"]) 
{
    [descarga.DownloadedList addObject:CurrentRestaurant];        
    CurrentRestaurant = nil;
    NSLog(@"Cerramos Retaurante");
    names = nil;
    
}
    
    rCurrentString = nil;
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    if (self.delegate != nil)
        [self.delegate ListOfRestaurantsDownloaded:[NSArray arrayWithArray: descarga.DownloadedList]];
    NSLog(@"TAMAÑO DE VECTOR: %d",[descarga.DownloadedList count]); 
    NSArray *path =
	NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    [NSKeyedArchiver archiveRootObject:(descarga.DownloadedList) toFile:[NSString stringWithFormat:@"%@/DownloadedBares",[path objectAtIndex:0]]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewRestaurants" object:nil];
    descarga.DownloadedList = nil;
}

@end
