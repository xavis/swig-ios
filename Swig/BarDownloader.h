//
//  BarDownloader.h
//  Swig
//
//  Created by Lion User on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Bar.h"
#import "Descarga.h"

@protocol BarDownloaderDelegate <NSObject>

- (void) ListOfRestaurantsDownloaded:(NSArray *)restaurantsList;

@end

@interface BarDownloader: NSObject <NSXMLParserDelegate>
{
    NSString *searchKeyString;
    NSMutableArray *searchKeyArray;
    NSURLConnection *mConnection;
    NSMutableData *mReceivedData;    
    Bar *CurrentRestaurant;
    Bar *OtherRestaurant;
    NSMutableString *rCurrentString;
    
    BOOL rIsDownloadingData;
}

- (void) downloadDataWithString:(NSString *) key;
- (void) downloadDataWithArray:(NSMutableArray *) key;

@property (nonatomic, strong) Descarga *descarga;
@property (nonatomic) BOOL *ready;
@property (nonatomic, weak) id <BarDownloaderDelegate>delegate;
@property (nonatomic, strong) NSMutableArray *names;

@end
