//
//  FavController.m
//  Swig
//
//  Created by Lion User on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FavController.h"
#import "Bar.h"
#import "Favourites.h"
#import "BarDetailView.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@implementation FavController

@synthesize FavouritesList;
@synthesize path;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationPortrait));
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    
    path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    path = [path stringByAppendingPathComponent:@"Favoritos"];
    [super viewDidLoad];
    /*self.title = @"Favoritos";
     [super viewDidLoad];
     UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 400, 44)];
     label.backgroundColor = [UIColor clearColor];
     label.font = [UIFont fontWithName:@"AmericanTypewriter-Bold" size:20];
     label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
     label.textAlignment = UITextAlignmentCenter;
     label.textColor =[UIColor whiteColor];
     label.text=self.title;  
     self.navigationItem.titleView = label;   */ 
    Favourites *favourites = [[Favourites alloc] init];
    favourites.FavouritesList = [[NSMutableArray alloc] init];
    if([NSKeyedUnarchiver unarchiveObjectWithFile:path]!=NULL){
        
        favourites.FavouritesList = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
        UISwipeGestureRecognizer *Right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
        [Right setDirection:(UISwipeGestureRecognizerDirectionRight )];
        [self.view addGestureRecognizer:Right];
        
    }
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
}


- (void)viewDidUnload
{ 
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];    
    self.title = @"Favoritos";
}

- (void)viewDidAppear:(BOOL)animated
{[self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    [super viewDidAppear:animated];
    
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;}



-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{

    cell.backgroundColor=[UIColor redColor];
    [cell setFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, 200)];}










- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    Favourites *favourites = [[Favourites alloc] init];
    favourites.FavouritesList = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    
    if([favourites.FavouritesList count]==0){
        UIView *back = [[UIView alloc] initWithFrame:self.view.frame];
        back.backgroundColor = [UIColor whiteColor];
        UIView *aviso = [[UIView alloc] initWithFrame:CGRectMake(30, 0, 260, 420)];
        aviso.backgroundColor= [UIColor grayColor];
        aviso.layer.cornerRadius=20;
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 120, 180, 80)];
        label.textColor = [UIColor whiteColor];
        label.textAlignment=UITextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        label.text=@"No tienes favoritos";
        [self.view addSubview:back];
        [back addSubview:aviso];
        [aviso addSubview:label];
        CABasicAnimation *opacidad = [CABasicAnimation animationWithKeyPath:@"opacity"];
        opacidad.duration=2.0;
        opacidad.repeatCount=1;
        opacidad.autoreverses=NO;
        opacidad.fromValue=[NSNumber numberWithFloat:0];
        opacidad.toValue=[NSNumber numberWithFloat:1.0];
        
        [aviso.layer addAnimation:opacidad forKey:@"AddOpacity"];
        

        
    }
    favourites.FavouritesList = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    UISwipeGestureRecognizer *Right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
    [Right setDirection:(UISwipeGestureRecognizerDirectionRight )];
    [self.view addGestureRecognizer:Right];
    return [favourites.FavouritesList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RestaurantsCell";
    
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==NULL){
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        // Configure the cell...
        
        UIImageView *imagen = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"star2.png"]];
        [imagen setFrame:CGRectMake(270,cell.frame.origin.y+5, 30, 30)];
        [cell addSubview:imagen];
        cell.backgroundColor = [UIColor redColor];
        UIView *backview = [[UIView alloc] initWithFrame:cell.frame];
        backview.backgroundColor = [UIColor orangeColor];
        cell.textLabel.backgroundColor = [UIColor yellowColor];
        cell.detailTextLabel.backgroundColor = nil;
        backview.layer.zPosition = (cell.textLabel.layer.zPosition)-1;
        
    }
    
    
  
    
    
    /* Restaurants *restaurant = [self.Restaurants objectAtIndex:indexPath.row];
     cell.titleLabel.text = restaurant.name;
     cell.adressLabel.text = restaurant.adress;*/
    
    Favourites *favourites = [[Favourites alloc] init];
    favourites.FavouritesList = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    
    
    Bar *res= [favourites.FavouritesList objectAtIndex:indexPath.row];
    
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",res.nombre];
    
    cell.detailTextLabel.text=[NSString stringWithFormat:@"%@",res.direccion];
    
    
    cell.tag= indexPath.row;
    
    //OPCION
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{/*
  // Navigation logic may go here. Create and push another view controller.
  
  Favourites *favourites = [[Favourites alloc] init];
  favourites.FavouritesList = [NSKeyedUnarchiver unarchiveObjectWithFile:@"Favoritos"];
  DetailView *detailViewController = [[DetailView alloc] initWithNibName:@"DetailView" bundle:nil];detailViewController.restaurante=[favourites.FavouritesList objectAtIndex:indexPath.row];
  // ...
  // Pass the selected object to the new view controller.
  [self.navigationController pushViewController:detailViewController animated:YES];*/
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    Favourites *favourites = [[Favourites alloc] init];
    favourites.FavouritesList = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    
    
    Bar *rest= [favourites.FavouritesList objectAtIndex:indexPath.row];
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    BarDetailView *newview = [[BarDetailView alloc] initWithNibName:@"BarDetailView" bundle:nil];
    newview.bar= rest;
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    [array addObject:self];
    appDelegate.ViewArray = [NSArray arrayWithArray:array];
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = newview;
                        [UIView setAnimationsEnabled:oldState];
                    } 
                    completion:nil];
    
}

-(void)handleSwipeRight:(UISwipeGestureRecognizer *)Right{
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    UIViewController *openview = [array objectAtIndex:array.count-1];
    
    
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = openview;
                        
                        [UIView setAnimationsEnabled:oldState];
                        
                        [array removeObjectAtIndex:array.count-1];
                        appDelegate.ViewArray = [NSArray arrayWithArray:array];
                        
                        
                    } 
                    completion:nil];}

@end
