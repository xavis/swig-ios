//
//  DealSearchResults.m
//  Swig
//
//  Created by Lion User on 31/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DealSearchResults.h"
#import "Deal.h"
#import "AppDelegate.h"
#import "DealDetailView.h"
@interface DealSearchResults ()

@end

@implementation DealSearchResults

@synthesize ScrollView;
@synthesize simpleview;
@synthesize xmlkeystring;
@synthesize xmlkeyarray;
@synthesize xml;
@synthesize Restaurants;
@synthesize pagina;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
-(id)initWithString:(NSString *) key NibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{ 
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    self.xmlkeystring = key;
    self.xmlkeyarray = nil;
    self.xml = [[DealDownloader alloc] init];
    if(xmlkeystring!=nil){
        [self.xml downloadDataWithString:xmlkeystring];
    }
    NSLog(@"%d",[self.Restaurants count]);
    pagina=1;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(someMethodToReloadTable:) name:@"NewRestaurants" object:nil];

    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationPortrait));
}
- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [ScrollView setContentSize:CGSizeMake(85, 420)];
    [ScrollView setFrame:CGRectMake(0,0,320,85)];
    ScrollView.backgroundColor = [UIColor grayColor];
    ScrollView.contentSize = CGSizeMake(450,ScrollView.frame.size.height);
    ScrollView.bounces=false;
    ScrollView.showsHorizontalScrollIndicator=true;
    ScrollView.userInteractionEnabled=true;
    UIButton *subut1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    subut1.frame = CGRectMake(12.5, 12.5, 60, 60);
    [ScrollView addSubview:subut1];
    
    UIButton *subut2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    subut2.frame = CGRectMake(85+12.5, 12.5, 60, 60);
    [ScrollView addSubview:subut2];
    
    UIButton *subut3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    subut3.frame = CGRectMake(85+60+25+12.5, 12.5, 60, 60);
    [ScrollView addSubview:subut3];
    
    UIButton *subut4 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    subut4.frame = CGRectMake(85+25+120+25+12.5, 12.5, 60, 60);
    [ScrollView addSubview:subut4];
    
    UIButton *subut5 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    subut5.frame = CGRectMake(85+25+180+25+25+12.5, 12.5, 60, 60);
    [ScrollView addSubview:subut5];
    
    UISwipeGestureRecognizer *Right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
    [Right setDirection:(UISwipeGestureRecognizerDirectionRight )];
    [self.view addGestureRecognizer:Right];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(self.pagina>1&&[self.Restaurants count]==10){
        return [self.Restaurants count]+2;}
    else if(pagina==1&&[self.Restaurants count]==10){
        return [self.Restaurants count]+1;
    }
    else if(pagina>1&&[self.Restaurants count]<10)
        return [self.Restaurants count]+1;
    else
        return [self.Restaurants count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RestaurantsCell";
    
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==NULL){
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    
    // Configure the cell...
    
    if(self.Restaurants.count != 0){
        if(indexPath.row<[self.Restaurants count]){
            Deal *res = [self.Restaurants objectAtIndex:indexPath.row];
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@",res.nombre];
            
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%@",res.bar.nombre];
            
            cell.tag= indexPath.row;}
        
        else if(indexPath.row==[self.Restaurants count]&&[self.Restaurants count]==10){
            
            Deal *last = [[Deal alloc] init];
            last.nombre = @"Cargar más...";
            last.bar.nombre= @"Como mucho 10 resultados más...";
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@",last.nombre];
            
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%@",last.bar.nombre];
            
            cell.tag= indexPath.row;
            
        }
        
        
        else {
            Bar *last = [[Bar alloc] init];
            last.nombre = @"Cargar anteriores...";
            last.direccion = @"Los 10 resultados anteriores...";
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@",last.nombre];
            
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%@",last.direccion];
            
            cell.tag= indexPath.row;}}
    
    
    //OPCION
    
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     DetailRestaurantViewController *detailViewController = [[DetailRestaurantViewController alloc] initWithRestaurant:[self.Restaurants objectAtIndex:indexPath.row]];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];*/
    
    if(indexPath.row<[self.Restaurants count]){
        
        UITableViewCell *cellda = [[UITableViewCell alloc] init];
        
        cellda.tag=indexPath.row;
        NSLog(@"YEEEEEEEPA");
        NSLog(@"%@", cellda);
        NSLog(@"%d",cellda.tag);
        NSLog(@"YEEEEEEEPA");
        Deal *rest = [self.Restaurants objectAtIndex: cellda.tag];
        UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        DealDetailView *newview = [[DealDetailView alloc] initWithNibName:@"DealDetailView" bundle:nil];
        newview.deal= rest;
        NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
        [array addObject:self];
        appDelegate.ViewArray = [NSArray arrayWithArray:array];
        
        
        [UIView transitionWithView:window2 
                          duration:0.5
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^(void) {
                            BOOL oldState = [UIView areAnimationsEnabled];
                            [UIView setAnimationsEnabled:NO];
                            window2.rootViewController = newview;
                            [UIView setAnimationsEnabled:oldState];
                        } 
                        completion:nil];}
    
    else if(indexPath.row==[self.Restaurants count]){
        NSLog(@"PENULTIMA SELECCIONADA");
        if([self.Restaurants count]==10){
            [self loadMore]; 
        }
        else{
            [self loadPrev];}
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reloadTable" object:nil];
        
    }
    
    else{NSLog(@"ULTIMA SELECCIONADA");
        [self loadPrev];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reloadTable" object:nil];}
    
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    /*if ([segue.identifier isEqualToString:@"PushRestaurantDetail"])
     {
     UITableViewCell *cell = (UITableViewCell *)sender;
     
     DetailRestaurantViewController *RestaurantDetailViewController = (DetailRestaurantViewController *)segue.destinationViewController;
     
     Restaurants *rest = [self.Restaurants objectAtIndex:cell.tag];
     RestaurantDetailViewController.restaurante = rest;
     }*/
    
    NSLog(@"WAKALA");
    if ([segue.identifier isEqualToString:@"Pokemon"])
    {NSLog(@"SUMA OTRO NSLOG");
        NSLog(@"SUMA OTRO NSLOG");
        
        
        
        
        
        
        
        NSLog(@"SUMA OTRO NSLOG");
    }
}

- (void)someMethodToReloadTable:(NSNotification *)notification 
{      
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Go" object:nil];
    NSLog(@"No Manda Notificación");
    self.Restaurants = [[NSMutableArray alloc] init];
    NSArray *path =
	NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.Restaurants = [NSKeyedUnarchiver unarchiveObjectWithFile:[NSString stringWithFormat:@"%@/DownloadedRestaurants",[path objectAtIndex:0]]];
    NSLog(@"RECARGA");
    [self.tableView reloadData];  
}  

- (void)loadMore{
    NSLog(@"Y pasa a LoadMore");
    self.pagina++;
    NSMutableArray *nuevacarga = [[NSMutableArray alloc] init];
    nuevacarga = self.Restaurants;
    NSString *nuevastring = [NSString stringWithFormat:@"%@&page=%d",self.xmlkeystring,self.pagina];
    DealSearchResults *ss = [DealSearchResults alloc];
    [ss initWithString:nuevastring NibName:@"DealSearchResults" bundle:nil];
    self.Restaurants == ss.Restaurants;
}

- (void)loadPrev{
    
    self.pagina--;
    NSLog(@"Y pasa a LoadPrev con pagina %d",self.pagina);
    NSString *nuevastring = xmlkeystring;
    if(self.pagina>1){
        NSLog(@"pasa al if");
        nuevastring = [NSString stringWithFormat:@"%@&page=%d",self.xmlkeystring,self.pagina];
        NSLog(@"%@",nuevastring);
    }
    DealSearchResults *ss = [DealSearchResults alloc];
    NSLog(@"%@",nuevastring);
    [ss initWithString:nuevastring NibName:@"DealSearchResults" bundle:nil];
    [self.Restaurants addObjectsFromArray:ss.Restaurants];
}

-(void)handleSwipeRight:(UISwipeGestureRecognizer *)Right{
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    UIViewController *openview = [array objectAtIndex:array.count-1];
    
    
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = openview;
                        
                        [UIView setAnimationsEnabled:oldState];
                        
                        [array removeObjectAtIndex:array.count-1];
                        appDelegate.ViewArray = [NSArray arrayWithArray:array];
                        
                        
                    } 
                    completion:nil];}

@end