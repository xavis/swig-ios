//
//  Descarga.m
//  Swig
//
//  Created by Lion User on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Descarga.h"

@implementation Descarga

@synthesize DownloadedList;
- (void)encodeWithCoder:(NSCoder *)coder {
    
    [coder encodeObject:self.DownloadedList forKey:@"Downloads"];
    
}

- (id)initWithCoder:(NSCoder *)coder {
    
    if ((self = [super init])) {
        
        self.DownloadedList = [coder decodeObjectForKey:@"Downloads"];
    }
    
    return self;
    
}

@end
