//
//  MainBarDownloader.h
//  Swig
//
//  Created by Lion User on 20/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Bar.h"
#import "Descarga.h"

@protocol MainBarDownloaderDelegate <NSObject>

- (void) ListOfRestaurantsDownloaded:(NSArray *)restaurantsList;

@end

@interface MainBarDownloader: NSOperation <NSXMLParserDelegate>
{
    NSString *searchKeyString;
    NSMutableArray *searchKeyArray;
    NSURLConnection *mConnection;
    NSMutableData *mReceivedData;    
    Bar *CurrentRestaurant;
    Bar *OtherRestaurant;
    NSMutableString *rCurrentString;
    NSOperationQueue *queue;
    
    BOOL rIsDownloadingData;
}

- (void) downloadDataWithString:(NSString *) key;
- (void) downloadDataWithArray:(NSMutableArray *) key;
-(id)initwithString:(NSString *)key;

@property (nonatomic, strong) Descarga *descarga;
@property (nonatomic) BOOL *ready;
@property (nonatomic, weak) id <MainBarDownloaderDelegate>delegate;
@property (nonatomic, strong) NSMutableArray *names;

@end