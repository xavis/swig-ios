//
//  BarSearchResults.m
//  Swig
//
//  Created by Lion User on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import "BarSearchResults.h"
#import "BarDetailView.h"
#import "AppDelegate.h"



@implementation BarSearchResults

@synthesize xmlkeystring;
@synthesize xmlkeyarray;
@synthesize xml;
@synthesize Restaurants;
@synthesize pagina;


-(id)initWithString:(NSString *) key{ 
    
    self = [super initWithStyle:UITableViewStylePlain];
    self.xmlkeystring = key;
    self.xmlkeyarray = nil;
    self.xml = [[BarDownloader alloc] init];
    if(xmlkeystring!=nil){
        [self.xml downloadDataWithString:xmlkeystring];
        }
    NSLog(@"%d",[self.Restaurants count]);
    pagina=1;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(someMethodToReloadTable:) name:@"NewRestaurants" object:nil];
    return self;
        
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationPortrait));
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    /* self.title = @"Resultados";
     [super viewDidLoad];
     UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 400, 44)];
     label.backgroundColor = [UIColor clearColor];
     label.font = [UIFont fontWithName:@"AmericanTypewriter-Bold" size:20];
     label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
     label.textAlignment = UITextAlignmentCenter;
     label.textColor =[UIColor whiteColor];
     label.text=self.title;  
     self.navigationItem.titleView = label; */
   
   

    self.title = @"Resultados";
    self.Restaurants = [[NSMutableArray alloc] init];
    NSArray *path =
	NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    self.Restaurants = [NSKeyedUnarchiver unarchiveObjectWithFile:[NSString stringWithFormat:@"%@/DownloadedRestaurants",[path objectAtIndex:0]]];
    UISwipeGestureRecognizer *Right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
    [Right setDirection:(UISwipeGestureRecognizerDirectionRight )];
    [self.view addGestureRecognizer:Right];

    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reloadTable" object:nil];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(self.pagina>1&&[self.Restaurants count]==10){
        return [self.Restaurants count]+2;}
    else if(pagina==1&&[self.Restaurants count]==10){
        return [self.Restaurants count]+1;
    }
    else if(pagina>1&&[self.Restaurants count]<10)
        return [self.Restaurants count]+1;
    else
        return [self.Restaurants count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RestaurantsCell";
    
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==NULL){
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    
    // Configure the cell...
    
    if(self.Restaurants.count != 0){
        if(indexPath.row<[self.Restaurants count]){
            Bar *res = [self.Restaurants objectAtIndex:indexPath.row];
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@",res.nombre];
            
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%@",res.direccion];
            
            cell.tag= indexPath.row;}
        
        else if(indexPath.row==[self.Restaurants count]&&[self.Restaurants count]==10){
            
            Bar *last = [[Bar alloc] init];
            last.nombre = @"Cargar más...";
            last.direccion = @"Como mucho 10 resultados más...";
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@",last.nombre];
            
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%@",last.direccion];
            
            cell.tag= indexPath.row;
            
        }
        
        
        else {
            Bar *last = [[Bar alloc] init];
            last.nombre = @"Cargar anteriores...";
            last.direccion = @"Los 10 resultados anteriores...";
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@",last.nombre];
            
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%@",last.direccion];
            
            cell.tag= indexPath.row;}}
    
    
    //OPCION
    
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     DetailRestaurantViewController *detailViewController = [[DetailRestaurantViewController alloc] initWithRestaurant:[self.Restaurants objectAtIndex:indexPath.row]];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];*/
    
    if(indexPath.row<[self.Restaurants count]){
        
        UITableViewCell *cellda = [[UITableViewCell alloc] init];
        
        cellda.tag=indexPath.row;
        NSLog(@"YEEEEEEEPA");
        NSLog(@"%@", cellda);
        NSLog(@"%d",cellda.tag);
        NSLog(@"YEEEEEEEPA");
        Bar *rest = [self.Restaurants objectAtIndex: cellda.tag];
        UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        BarDetailView *newview = [[BarDetailView alloc] initWithNibName:@"BarDetailView" bundle:nil];
        newview.bar= rest;
        NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
        self.Restaurants=Restaurants;
        [array addObject:self];
        appDelegate.ViewArray = [NSArray arrayWithArray:array];
        
        
        [UIView transitionWithView:window2 
                          duration:0.5
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^(void) {
                            BOOL oldState = [UIView areAnimationsEnabled];
                            [UIView setAnimationsEnabled:NO];
                            window2.rootViewController = newview;
                            [UIView setAnimationsEnabled:oldState];
                        } 
                        completion:nil];}
    
    else if(indexPath.row==[self.Restaurants count]){
        NSLog(@"PENULTIMA SELECCIONADA");
        if([self.Restaurants count]==10){
            [self loadMore]; 
        }
        else{
            [self loadPrev];}
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reloadTable" object:nil];
        
    }
    
    else{NSLog(@"ULTIMA SELECCIONADA");
        [self loadPrev];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reloadTable" object:nil];}
    
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    /*if ([segue.identifier isEqualToString:@"PushRestaurantDetail"])
     {
     UITableViewCell *cell = (UITableViewCell *)sender;
     
     DetailRestaurantViewController *RestaurantDetailViewController = (DetailRestaurantViewController *)segue.destinationViewController;
     
     Restaurants *rest = [self.Restaurants objectAtIndex:cell.tag];
     RestaurantDetailViewController.restaurante = rest;
     }*/
    
    NSLog(@"WAKALA");
    if ([segue.identifier isEqualToString:@"Pokemon"])
    {NSLog(@"SUMA OTRO NSLOG");
        NSLog(@"SUMA OTRO NSLOG");
        
 

        
        
        
        
        NSLog(@"SUMA OTRO NSLOG");
    }
}

- (void)someMethodToReloadTable:(NSNotification *)notification 
{      
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Go" object:nil];
    NSLog(@"No Manda Notificación");
    self.Restaurants = [[NSMutableArray alloc] init];
    NSArray *path =
	NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.Restaurants = [NSKeyedUnarchiver unarchiveObjectWithFile:[NSString stringWithFormat:@"%@/DownloadedBares",[path objectAtIndex:0]]];
    NSLog(@"RECARGA");
    [self.tableView reloadData];  
}  

- (void)loadMore{
    NSLog(@"Y pasa a LoadMore");
    self.pagina++;
    NSMutableArray *nuevacarga = [[NSMutableArray alloc] init];
    nuevacarga = self.Restaurants;
    NSString *nuevastring = [NSString stringWithFormat:@"%@&page=%d",self.xmlkeystring,self.pagina];
    BarSearchResults *ss = [BarSearchResults alloc];
    [ss initWithString:nuevastring];
    self.Restaurants == ss.Restaurants;
}

- (void)loadPrev{
    
    self.pagina--;
    NSLog(@"Y pasa a LoadPrev con pagina %d",self.pagina);
    NSString *nuevastring = xmlkeystring;
    if(self.pagina>1){
        NSLog(@"pasa al if");
        nuevastring = [NSString stringWithFormat:@"%@&page=%d",self.xmlkeystring,self.pagina];
        NSLog(@"%@",nuevastring);
    }
    BarSearchResults *ss = [BarSearchResults alloc];
    NSLog(@"%@",nuevastring);
    [ss initWithString:nuevastring];
    [self.Restaurants addObjectsFromArray:ss.Restaurants];
}

-(void)handleSwipeRight:(UISwipeGestureRecognizer *)Right{
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    UIViewController *openview = [array objectAtIndex:array.count-1];
    
    
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = openview;
                        
                        [UIView setAnimationsEnabled:oldState];
                        
                        [array removeObjectAtIndex:array.count-1];
                        appDelegate.ViewArray = [NSArray arrayWithArray:array];
                        
                        
                    } 
                    completion:nil];}

@end