//
//  DealSearchResults.h
//  Swig
//
//  Created by Lion User on 31/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DealDownloader.h";

@interface DealSearchResults : UITableViewController

@property (nonatomic, weak) IBOutlet UITableView *simpleview;

@property (nonatomic, weak) IBOutlet UIScrollView *ScrollView;

@property (nonatomic, strong) NSString *xmlkeystring;
@property (nonatomic, strong) NSMutableArray *xmlkeyarray;
@property (nonatomic, strong) DealDownloader *xml;
@property (nonatomic, strong) NSMutableArray *Restaurants;
@property (nonatomic, assign) int pagina;
-(id)initWithString:(NSString *) key NibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;
-(id) initWithArray:(NSMutableArray *) key;

- (void)someMethodToReloadTable:(NSNotification *)notification;
- (void)loadMore;
- (void)loadPrev;

-(void)handleSwipeRight:(UISwipeGestureRecognizer *)Right;


@end