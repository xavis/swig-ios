//
//  FavController.h
//  Swig
//
//  Created by Lion User on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Favourites.h"

@interface FavController : UITableViewController

@property (strong,nonatomic) NSString *path;
@property (strong, nonatomic) NSMutableArray *FavouritesList;

-(void)handleSwipeRight:(UISwipeGestureRecognizer *)Right;

@end
