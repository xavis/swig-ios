//
//  DealDetailView.m
//  Swig
//
//  Created by Lion User on 01/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DealDetailView.h"
#import "AppDelegate.h"
#import "BarDetailView.h"

@interface DealDetailView ()

@end

@implementation DealDetailView
@synthesize back;
@synthesize image;
@synthesize home;
@synthesize horario;
@synthesize desc;
@synthesize dealtitle;
@synthesize fecha;
@synthesize goBar;
@synthesize deal;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if(image){
        
    //CARGARIMAGEN
    
    }
    else{
        image = nil;}
    if(self.deal.descripcion&&!(self.deal.descripcion.length<5)){
        desc.text = self.deal.descripcion;}
    else{  desc.text = @"Sin datos";   } 
    if(self.deal.nombre&&!(self.deal.nombre
       .length<5)){dealtitle.text = self.deal.nombre;}
    else{dealtitle.text = @"Sin Título";}
    if(self.deal.horario&&!(self.deal.horario.length<5)){
        horario.text = [NSString stringWithFormat:@"%@%@",@"- ",self.deal.horario];}
    else{horario.text = @"- Horario sin definir";}
    if(self.deal.fechafin&&!(self.deal.fechafin.length<5)){fecha.text = [NSString stringWithFormat:@"%@%@",@"- ",self.deal.fechafin];}
    else{fecha.text=@"- Fecha sin definir";}    
    UISwipeGestureRecognizer *Right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
    [Right setDirection:(UISwipeGestureRecognizerDirectionRight )];
    [self.view addGestureRecognizer:Right];
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationPortrait));
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



-(void)handleSwipeRight:(UISwipeGestureRecognizer *)Right{
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    UIViewController *openview = [array objectAtIndex:array.count-1];
    
    
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = openview;
                        
                        [UIView setAnimationsEnabled:oldState];
                        
                        [array removeObjectAtIndex:array.count-1];
                        appDelegate.ViewArray = [NSArray arrayWithArray:array];
                        
                        
                    } 
                    completion:nil];}

-(IBAction)homeAction:(id)sender{
    
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    UIViewController *openview = [array objectAtIndex:0];
    
    
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = openview;
                        
                        [UIView setAnimationsEnabled:oldState];
                        
                        [array removeObjectAtIndex:array.count-1];
                        appDelegate.ViewArray = nil;
                        
                    } completion:nil]; } 


-(IBAction)back:(id)sender{
    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    UIViewController *openview = [array objectAtIndex:array.count-1];
    
    
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = openview;
                        
                        [UIView setAnimationsEnabled:oldState];
                        
                        [array removeObjectAtIndex:array.count-1];
                        appDelegate.ViewArray = [NSArray arrayWithArray:array];
                        
                        
                    } 
                    completion:nil];}

-(IBAction)goToBar:(id)sender{

    UIWindow* window2 = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSMutableArray *array = [NSMutableArray arrayWithArray:appDelegate.ViewArray];
    BarDetailView *openview = [[BarDetailView alloc] init];
    openview.bar = self.deal.bar;
    
    
    [UIView transitionWithView:window2 
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        window2.rootViewController = openview;
                        
                        [UIView setAnimationsEnabled:oldState];
                        
                        [array removeObjectAtIndex:array.count-1];
                        appDelegate.ViewArray = [NSArray arrayWithArray:array];
                        
                        
                    } 
                    completion:nil];


}

@end
