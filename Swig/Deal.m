//
//  Deal.m
//  Swig
//
//  Created by Lion User on 31/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Deal.h"

@implementation Deal

@synthesize nombre;
@synthesize tipo;
@synthesize horario;
@synthesize  fechafin;
@synthesize bar;
@synthesize descripcion;
@synthesize imagen;



- (void)encodeWithCoder:(NSCoder *)coder {
    
    [coder encodeObject:self.nombre forKey:@"nombre"];
    
    [coder encodeObject:self.tipo forKey:@"tipo"];
    
    [coder encodeObject:self.bar forKey:@"bar"];
    
    [coder encodeObject:self.horario forKey:@"horario"];
    
    [coder encodeObject:self.fechafin forKey:@"fechafin"];
    
    [coder encodeObject:self.descripcion forKey:@"descripcion"];
    
    [coder encodeObject:self.imagen forKey:@"imagen"];
    
    
    
}

- (id)initWithCoder:(NSCoder *)coder {
    
    if ((self = [super init])) {
        
        self.nombre = [coder decodeObjectForKey:@"nombre"];
        
        self.tipo = [coder decodeObjectForKey:@"tipo"];
        
        self.horario = [coder decodeObjectForKey:@"horario"];
        
        self.fechafin = [coder decodeObjectForKey:@"fechafin"];
        
        self.bar = [coder decodeObjectForKey:@"bar"];

        self.descripcion = [coder decodeObjectForKey:@"descripcion"];
        
        self.imagen = [coder decodeObjectForKey:@"imagen"];
        
    }
    
    return self;
    
}




@end
