//
//  Bar.m
//  Swig
//
//  Created by Lion User on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Bar.h"

@implementation Bar

@synthesize nombre;
@synthesize tipo;
@synthesize direccion;
@synthesize localidad;
@synthesize provincia;
@synthesize latitud;
@synthesize longitud;
@synthesize telefono;
@synthesize imagen;
@synthesize barid;



- (void)encodeWithCoder:(NSCoder *)coder {
    
    [coder encodeObject:self.nombre forKey:@"nombre"];
    
    [coder encodeObject:self.tipo forKey:@"tipo"];
    
    [coder encodeObject:self.localidad forKey:@"localidad"];
    
    [coder encodeObject:self.provincia forKey:@"provincia"];
    
    [coder encodeObject:self.direccion forKey:@"direccion"];
    
    [coder encodeObject:self.latitud forKey:@"latitud"];
    
    [coder encodeObject:self.longitud forKey:@"longitud"];
    
    [coder encodeObject:self.telefono forKey:@"telefono"];
    
    [coder encodeObject:self.barid forKey:@"barid"];
    
    [coder encodeObject:self.imagen forKey:@"imagen"];

    
}

- (id)initWithCoder:(NSCoder *)coder {
    
    if ((self = [super init])) {
        
        self.nombre = [coder decodeObjectForKey:@"nombre"];
        
        self.tipo = [coder decodeObjectForKey:@"tipo"];
        
        self.localidad = [coder decodeObjectForKey:@"localidad"];
        
        self.provincia = [coder decodeObjectForKey:@"provincia"];
        
        self.direccion = [coder decodeObjectForKey:@"direccion"];
        
        self.latitud = [coder decodeObjectForKey:@"latitud"];
        
        self.longitud = [coder decodeObjectForKey:@"longitud"];
        
        self.telefono = [coder decodeObjectForKey:@"telefono"];
        
        self.barid = [coder decodeObjectForKey:@"barid"];
        
        self.imagen = [coder decodeObjectForKey:@"imagen"];
        

    }
    
    return self;
    
}




@end
